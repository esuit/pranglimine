import React, {Component} from 'react';
import ReactDOM from 'react-dom'
import {HotKeys} from 'react-hotkeys';
import './App.css';

class MessageHub extends Component {
  constructor(){
    super()
    this.state = {
      messages:[],
      receivers:[]
    }
  }

  deliverMessages(){
    this.setState((state,props)=>{
      const message = state.messages.pop();
      state.receivers[message.messageType](message.payload)
    })
  }

  receiveMessage(messageType, payload){
    this.setState((state,props)=>{
      state.messages.push({messageType:payload})
      return state
    },()=>this.deliverMessages())

  }

  registerReceiver(messageType,receiver){
    this.setState((state,props)=>{
      state.receivers.push({messageType:receiver})
      return state
    })
  }
}
class App extends Component {
  getLevels() {
    return {
      0: {
        operations: "+-",
        numbersMax: 9,
        numbersMin: 1,
        answerMax: 9,
        answerMin: 1,
        onlyInteger: true,
        rightAnswerPoints: 2,
        wrongAnswerPoints: -5,
        rightAnswerAddTime:10,
        levelUpPoints: 20
      },
      4: {
        operations: "+-",
        numbersMax: 9,
        numbersMin: 1,
        answerMax: 20,
        answerMin: 1,
        onlyInteger: true,
        rightAnswerPoints: 5,
        wrongAnswerPoints: -5,
        rightAnswerAddTime:8,
        levelUpPoints: 30
      },
      8: {
        operations: "*",
        numbersMax: 5,
        numbersMin: 1,
        answerMax: 25,
        answerMin: 1,
        rightAnswerPoints: 20,
        wrongAnswerPoints: -5,
        rightAnswerAddTime:3,
        levelUpPoints: 100
      }
    }
  }
  render() {
    const appStyle = {
      position: "absolute",
      left: "40%",
      top: "15px"
    }
    return (<div className="App" style={appStyle}>
      <MentalMath levelUpPoints="100" gameTime="90" startingLevel="1" gameLevels={this.getLevels()}></MentalMath>
    </div>);
  }
}

export default App;

class MentalMath extends Component {

  constructor(props) {
    super(props);
    this.state = {
      answer: "",
      isDisabled: true,
      operation: "",
      currentLevel: props.startingLevel,
      points: 0,
      pointsOnThisLevel: 0,
      rightAnswersThisLevel: 0,
      wrongAnswersThisLevel: 0,
      CLR: this.getCurrentLevelRules(props.startingLevel, props.gameLevels),
      levelStats: []
    }
    this.clickFunction = this.clickFunction.bind(this);
    this.clearFunction = this.clearFunction.bind(this);
    this.submitFunction = this.submitFunction.bind(this);
    this.timerStart = this.timerStart.bind(this);
    this.timerStop = this.timerStop.bind(this);
    this.timerEnd = this.timerEnd.bind(this);
  }

  clickFunction(a) {
    const key = (
      (typeof a === "object")
      ? a.key
      : a);
    this.setState(function(state, props) {
      return {
        answer: (key === '-')
          ? key + state.answer
          : state.answer + key
      }
    })
  }

  clearFunction() {
    this.setState({answer: ""})
  }

  tabCatch(e) {
    e.preventDefault() // this is just here to cach TAB from moving focus
  }

  timerStart(a) {
    this.setState({
      answer: "",
      isDisabled: false,
      operation: this.generateOperation(),
      levelStats: [],
      pointsOnThisLevel: 0,
      rightAnswersThisLevel: 0,
      wrongAnswersThisLevel: 0
    })
  }

  timerStop(a) {
    this.setState({isDisabled: true, operation: ""})
    this.gameOver();
  }

  timerEnd(a) {
    this.setState({isDisabled: true, operation: ""})
    this.gameOver();
  }

  gameOver() {
    console.error("GAME OVER!");
    console.log(this.state.levelStats);
  }

  submitFunction() {
    const {answer, operation} = this.state
    if (eval(operation) == answer) {
      this.gameProgression(true);
      this.setState({answer: "", operation: this.generateOperation()})
    } else {
      this.gameProgression(false)
      this.setState({answer: "", operation: this.generateOperation()})
    }
  }

  gameProgression(hadRightAnswer) {
    let {points, pointsOnThisLevel, rightAnswersThisLevel, wrongAnswersThisLevel} = this.state
    const {rightAnswerPoints, wrongAnswerPoints, levelUpPoints} = this.state.CLR
    if (hadRightAnswer) {
      rightAnswersThisLevel++;
      points = points + rightAnswerPoints;
      pointsOnThisLevel = pointsOnThisLevel + rightAnswerPoints;
      const addedTime = this.state.CLR.rightAnswerAddTime;
      this.setState({points, pointsOnThisLevel, rightAnswersThisLevel, addedTime})

      if (pointsOnThisLevel % levelUpPoints === 0) {
        this.levelUp({rightAnswers: rightAnswersThisLevel, wrongAnswers: wrongAnswersThisLevel, points: pointsOnThisLevel});
      }

    } else if (points > Math.abs(wrongAnswerPoints)) {
      points = points + wrongAnswerPoints
      wrongAnswersThisLevel++
      this.setState({points, wrongAnswersThisLevel})
    }
  }

  levelUp(stats) {
    const {gameLevels} = this.props
    let {levelStats, currentLevel} = this.state
    levelStats.push(stats)
    currentLevel++
    this.setState({
      CLR: this.getCurrentLevelRules(currentLevel, gameLevels),
      pointsOnThisLevel: 0,
      rightAnswersThisLevel: 0,
      wrongAnswersThisLevel: 0,
      currentLevel,
      levelStats
    })
  }

  generateOperation() {
    const random = this.randomInBetween
    const {CLR, currentLevel} = this.state

    let counter = 0;
    let answer = null;
    let op = "";

    while (counter <= 1000 && (answer < CLR.answerMin || answer > CLR.answerMax || answer == this.state.answer)) {
      op = random(CLR.numbersMin, CLR.numbersMax, CLR.onlyInteger) + " " + this.randomOp(CLR.operations) + " " + random(CLR.numbersMin, CLR.numbersMax);
      answer = eval(op)
      counter++
    }
    console.log("answer " + answer)
    if (counter === 1000) {
      console.error("Could not make up operation according to rules of level " + currentLevel + ", sorry!")
      console.error("Current level rules were: ", CLR);
    } else {
      //console.log("next op:"+op)
    }
    return op
  }

  getCurrentLevelRules(currentLevel, levels) {
    while ((typeof levels[currentLevel] === "undefined") && (currentLevel > 0)) {
      currentLevel--
    }
    return levels[currentLevel];
  }

  randomInBetween(a, b) {
    return Math.floor(Math.random() * b) + a
  }

  randomOp(ops) {
    return ops[this.randomInBetween(0, ops.length)]
  }

  render() {
    const keyMap = {
      'clear': [
        'del', 'backspace', 'esc'
      ],
      'enter': 'enter',
      'number': [
        '1',
        '2',
        '3',
        '4',
        '5',
        '6',
        '7',
        '8',
        '9',
        '0'
      ],
      'special': [
        '-', ',', '.'
      ],
      'tab': 'tab'
    }
    const handlers = {
      'clear': this.clearFunction,
      'enter': this.submitFunction,
      'number': this.clickFunction,
      'special': this.clickFunction,
      'tab': this.tabCatch
    };
    const {isDisabled, operation} = this.state
    const levelStats = this.state.levelStats
    const {gameTime} = this.props
    const mmStyle = {
      width: "300px"
    }

    const hiddenStyle = {
      display: "none"
    }

    return <div className="MentalMath" style={mmStyle}>
      <HotKeys keyMap={keyMap} handlers={handlers}>
        <MmTimer isDisabled={isDisabled} exerciseTime={gameTime} timerStart={this.timerStart} timerStop={this.timerStop} timerEnd={this.timerEnd} addedTime={this.state.addedTime} ref="mmtimer"></MmTimer>
        <MathOp operation={operation} answer={this.state.answer}></MathOp>
        <Points points={this.state.points} level={this.state.currentLevel} maxPoints={this.state.CLR.levelUpPoints}></Points>
        <NumberPad isDisabled={isDisabled} clickFunction={this.clickFunction} submitFunction={this.submitFunction} clearFunction={this.clearFunction}></NumberPad>
        <LevelStats stats={levelStats} isDisabled={isDisabled}></LevelStats>
      </HotKeys>
    </div>
  }
}

class Points extends Component {
  render() {
    const {points, level} = this.props
    if (points > 0) {
      return <Prize max={this.props.maxPoints} current={points}>Punkte: {points}, Tase: {level}</Prize>
    } else {
      return <p>Tase : {level}</p>
    }
  }
}

class MmTimer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      timeLeft: this.props.exerciseTime,
      timerRunning: false,
      timerHandle: null,
      addedTime:this.props.addedTime,
    }

    this.timerONOFFFunction = this.timerONOFFFunction.bind(this)
    this.timerTick = this.timerTick.bind(this)
  }

  timerTick() {
    if (this.state.timerRunning) {
      this.setState(function(state, props) {
        if(props.addedTime>0) {
          state.timeLeft = state.timeLeft+props.addedTime;
        } else {
          state.timeLeft--;
        }
        if (state.timeLeft === 0) {
          state.timerRunning = false;
          if (typeof props.timerEnd === 'function') {
            props.timerEnd(true);
          }
        }
        return state;
      })
    }
  }
  timerONOFFFunction() {
    const {timerRunning, timerHandle} = this.state
    if (!timerRunning) {
      this.setState({
        timerRunning: true,
        timeLeft: this.props.exerciseTime,
        timerHandle: setInterval(this.timerTick, 1000)
      })
      if (typeof this.props.timerStart === "function") {
        this.props.timerStart(this.props.exerciseTime)
      }
    } else {
      clearInterval(timerHandle);
      this.setState({timerRunning: false})
      if (typeof this.props.timerStop === "function") {
        this.props.timerStop(this.state.timeLeft)
      }

    }
  }

  render() {
    const {timeLeft, timerRunning} = this.state
    return (<div className="mmTimer">
      <Button buttonValue="timerStart" clickFunction={this.timerONOFFFunction}>{
          timerRunning
            ? <Intl lang="et" msgId="btnBegin_stop"></Intl>
            : <Intl lang="et" msgId="btnBegin_st"></Intl>
        }</Button>
      <TimeDisplay isDisabled={this.props.isDisabled} timeLeft={timeLeft}></TimeDisplay>
    </div>)
  }
}

class TimeDisplay extends Component {
  render() {
    const {timeLeft} = this.props
    if (this.props.isDisabled) {
      return ""
    } else {
      const timerStyle = {
        color: (timeLeft > 10)
          ? "green"
          : "red"
      }
      const inputStyle = {
        width: "auto",
        maxWidth: "30px",
        border: "none",
        textAlign: "right"
      }
      return <div className="timerDisplay" style={timerStyle}><input autoFocus="autoFocus" ref={(input) => {
          this.timer = input;
        }} style={inputStyle} readOnly="readOnly" value={timeLeft}/>
        sekundit veel</div>
    }
  }
}

class MathOp extends Component {
  render() {
    const {operation, answer} = this.props
    if (operation !== "") {
      return <div className="MathOp">
        <p>{operation}
          = {answer}</p>
      </div>
    } else
      return ""
  }
}

class NumberPad extends Component {
  render() {
    const {isDisabled} = this.props
    const tableStyle = {
      textAlign: "left"
    }
    const warnButtonStyle = {
      backgroundColor: isDisabled
        ? "gray"
        : "red",
      height: "60px",
      width: "60px"

    }
    const normButtonStyle = {
      backgroundColor: isDisabled
        ? "gray"
        : "green",
      height: "60px",
      width: "60px"
    }
    if (isDisabled) {
      return <p>Vajuta [Alusta]</p>
    } else
      return <div id="compuTable" style={tableStyle}>
        <Button {...this.props} buttonStyle={normButtonStyle}>7</Button>
        <Button {...this.props} buttonStyle={normButtonStyle}>8</Button>
        <Button {...this.props} buttonStyle={normButtonStyle}>9</Button>
        <Button {...this.props} buttonStyle={normButtonStyle}>,</Button>
        <br/>
        <Button {...this.props} buttonStyle={normButtonStyle}>4</Button>
        <Button {...this.props} buttonStyle={normButtonStyle}>5</Button>
        <Button {...this.props} buttonStyle={normButtonStyle}>6</Button>
        <br/>
        <Button {...this.props} buttonStyle={normButtonStyle}>1</Button>
        <Button {...this.props} buttonStyle={normButtonStyle}>2</Button>
        <Button {...this.props} buttonStyle={normButtonStyle}>3</Button>
        <br/>
        <Button {...this.props} buttonStyle={normButtonStyle}>0</Button>
        <Button {...this.props} buttonStyle={normButtonStyle} clickFunction={this.props.submitFunction}>OK</Button>
        <Button {...this.props} buttonStyle={warnButtonStyle} clickFunction={this.props.clearFunction}>Tühista</Button>
        <br/>
      </div>
  }
}

class LevelStats extends Component {
  render() {
    const {isDisabled, stats} = this.props
    if (isDisabled && typeof stats !== "undefined" && stats.length > 0) {
      return <div className="statistics">{JSON.stringify(this.props.stats)}</div>
    } else {
      return ""
    }
  }
}
class Button extends Component {
  constructor(props) {
    super(props)
    this.handleOnClick = this.handleOnClick.bind(this);
  }
  handleOnClick(e) {
    if ((!this.props.isDisabled) && (typeof this.props.clickFunction === "function")) {
      this.props.clickFunction(this.props.children)
    }
  }
  render() {
    let buttonStyle = {}
    Object.assign(buttonStyle, this.props.buttonStyle);
    return <button style={buttonStyle} onClick={this.handleOnClick}>{this.props.children}</button>
  }
}

class Intl extends Component {
  constructor() {
    super()
    this.state = {
      "en" : {
      "btnBegin_start" : "Begin",
      "btnBegin_stop" : "Stop",
     },
      "et" : {
        "btnBegin_st" : "Alusta",
        "btnBegin_stop" : "Lõpeta",
      }
    }
  }
  render(){
    const {lang,msgId}=this.props
    const msg=this.state[lang][msgId]
    return <span>{msg}</span>
  }
}

class Prize extends Component {
  render(){
    const {max,current}=this.props
    const baseStyle={
      position:"absolute",
      border:"1px solid black",
      width:"200px",
      height:"20px",
    }
    const bgStyle={
      backgroundColor:"white"
    }
    const progressHeight = 100*current/200;
    const progressStyle={
      position:"relative",
      backgroundColor:"green",
      width:progressHeight+"px",
      height:"20px",

    }

    return <div className="progress" style={baseStyle} style={bgStyle}><div className="current" style={progressStyle}></div>{this.props.children}</div>
  }

}
